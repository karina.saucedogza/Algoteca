A Java Web Application where a user can maintain a collection of music, literature and other types of media he owns. This project was done for my Object Oriented Design class. It is a very early version and its aim was to apply design patterns on an application.
Both the code and the application are completely in Spanish.
Captures:
<h5>Login</h5>
![image](/uploads/7b23031a1edc9b87307c6de11a068649/image.png)
<h5>Sign up</h5>
![image](/uploads/3da5d300cd6ac9eb26d4b1bd374d8393/image.png)
<h5>Home</h5>
![image](/uploads/a157d1dbf17d69604ca30fde6d7f093e/image.png)
<h5>New article</h5>
![image](/uploads/340d8c0455d541b5164631d62dedac14/image.png)
<h5>Search</h5>
![image](/uploads/43a521be3acf76e3b98ea4faf6dff427/image.png)
<h5>Article details</h5>
![image](/uploads/429af656636b407aeefa321110b20a63/image.png)
You can also modify and delete on the same page
<h5>Modifying</h5>
![image](/uploads/faa8ccf373529b7560197576be549a47/image.png)
<h5>Deleting</h5>
![image](/uploads/c1424c712f86cf10a129748df3a5189c/image.png)
Running a search
![image](/uploads/dc060b227cac35b6b7a368d0aa00cbca/image.png)
<h5>Add category</h5>
![image](/uploads/43ec626c3e3e63e6c754376996eb5383/image.png)
Category added
![image](/uploads/fd2a446cd0cd8d3f60a8aa4d9c059015/image.png)
<h5>Generic error page</h5>
![image](/uploads/b549a37727a2f7531979388ac0a046fc/image.png)

