<%-- 
    Document   : AgregarCategoria
    Created on : 3/05/2016, 04:02:06 PM
    Author     : Principal
--%>

<%@page import="biblioteca.CategoriasPOJO"%>
<%@page import="java.util.List"%>
<%@page import="biblioteca.CategoriasDAODBImpl"%>
<%@page import="biblioteca.CategoriasDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Algoteca</title>
        <link rel="stylesheet" href="Estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis|Ubuntu" rel="stylesheet"> 
    </head>
    
    <%String usuario=(String)session.getAttribute("usuario");
       %>
       <div id="banner">&emsp;¡Hola, <%=usuario%>!<a id="logout" href="logout.jsp">Cerrar Sesión</a></div>
       
       <br><br>
    <body background="background.jpg">
        <div id="registroCat">
            <a href="consulta.jsp" title="Regresar"><img src="goback.png"></a>
            <div class="row2">
             <div class="column2">
            <h2>Categorías existentes</h2>
            <table border="1">
                <tr>
                    <th>Clave</th>
                    <th>Categoría</th>
                </tr>
            <%CategoriasDAO c=new CategoriasDAODBImpl();
           List categorias=c.mostrarTodas();
           for(Object o:categorias){
               CategoriasPOJO categoria=(CategoriasPOJO)o;
         %>
         <tr>
             <td><%=categoria.getIdCategoria()%></td>
             <td><%=categoria.getCategoria()%></td>
         </tr>
      <%}%>
        </table>
        </div>
        <div class="column2">
   <h2>Nueva categoría</h2>
        <form action="BibliotecaControlador" method="POST">
            <br>
            Clave: &emsp;&emsp;<input type="text" name="clave" required>
            <br> <br>
            Categoría: <input type="text" name="nombre" required>
            <br> <br><br>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="submit" value="Guardar" id="boton">
            <br><br>
            <input type="hidden" value="registroCategoria" name="accion">
        </form>
        </div>
            </div>
        </div>
    </body>
</html>
