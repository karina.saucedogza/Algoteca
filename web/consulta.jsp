<%-- 
    Document   : consulta
    Created on : 28/03/2016, 12:21:06 PM
    Author     : Karina Saucedo Garza
--%>

<%@page import="biblioteca.CategoriasPOJO"%>
<%@page import="biblioteca.CategoriasDAODBImpl"%>
<%@page import="biblioteca.CategoriasDAO"%>
<%@page import="biblioteca.ArticulosPOJO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Algoteca</title>
         <link rel="stylesheet" href="Estilo.css">
         <link href="https://fonts.googleapis.com/css?family=Dosis|Ubuntu" rel="stylesheet"> 
    </head>
   
    <body background="background.jpg">
       <%String usuario=(String)session.getAttribute("usuario");
       %>
       <div id="banner">&emsp;¡Hola, <%=usuario%>!<a id="logout" href="logout.jsp">Cerrar Sesión</a></div>
       <div id="entrada">
        <h2>Mi biblioteca</h2>
        
        <form action="BibliotecaControlador" method="POST">
            <input type="text" name="busqueda" required> <input id="boton" type="submit" value="Buscar">
            <input type="hidden" name="accion" value="busqueda">
        </form><br>
         <a id="enlace" href="AgregarArticulo.jsp">Agregar Artículo</a>
        <br><br>
        <a id="enlace" href="AgregarCategoria.jsp">Agregar Categoría</a>
       </div>
        <br>
        <%if(session!=null){
           List articulos=(List)session.getAttribute("busqueda");
           if(articulos!=null){
       %>
       <table border="1">
                <tr>
                    <th>Clave</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Categoría</th>
                    <th>Acciones</th>
                </tr>
      <% for(Object o:articulos){
          ArticulosPOJO articulo=(ArticulosPOJO) o;
          String nombreCategoria="";
      %>
      <%CategoriasDAO c=new CategoriasDAODBImpl();
                        List categorias=c.mostrarTodas();
                        for(Object obj:categorias){
                            CategoriasPOJO categoria=(CategoriasPOJO)obj;
                            if(articulo.getId_Categoria()==categoria.getIdCategoria()){
                                nombreCategoria=categoria.getCategoria();
                            }
                        }%>
          <tr>
                    <td><%=articulo.getId_Articulo()%></td>
                    <td><%=articulo.getTitulo()%></td>
                    <td><%=articulo.getDescripcion()%></td>
                    <%System.out.println(articulo.getDescripcion());%>
                    <td><%=nombreCategoria%></td>
                    <%String art=articulo.getId_Articulo();%>
                    <td><a href="detalles.jsp?articulo=<%=art%>">Detalles</a></td>
                    
          </tr>
        <% } %>       
        </table>
    <%  }
      }
    %>
        <br>
       
    </body>
</html>
