<%-- 
    Document   : RegistroUsuario
    Created on : 28/03/2016, 12:11:46 PM
    Author     : Karina Guadalupe Saucedo Garza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Usuario</title>
        <link rel="stylesheet" href="Estilo.css">
	<link href="https://fonts.googleapis.com/css?family=Dosis|Ubuntu" rel="stylesheet"> 
    </head>
    
    <body background="background.jpg">
        <div id="banner">&emsp;Algoteca&emsp;<a href="index.html"><img src="home.png"></a></div>
         
        <form id="entrada" action="BibliotecaControlador" method="POST">
        <div>
	   <h2>Registro</h2>
           
            &emsp;Usuario: <input id="usuario" type="text" name="usuario" required><br><br>
            &emsp;Contraseña: <input id="password" type="password" name="pass" required>
            <br><br>
            &emsp;Introduce tu nombre: <input id="nombre" type="text" name="nombre" required>
            &emsp;<input id="boton" type="reset" value="Limpiar">&emsp;<input id="boton" type="submit" value="Registrar">
          
            <input type="hidden" name="accion" value="registro">
            
            <br><br><br>
        </div>
        </form>
    </body>
</html>
