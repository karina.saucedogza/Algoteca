<%-- 
    Document   : detalles
    Created on : 27/04/2016, 11:42:01 PM
    Author     : Karina Saucedo Garza
--%>

<%@page import="biblioteca.CategoriasPOJO"%>
<%@page import="biblioteca.CategoriasDAODBImpl"%>
<%@page import="biblioteca.CategoriasDAO"%>
<%@page import="biblioteca.ArticulosPOJO"%>
<%@page import="java.util.List"%>
<%@page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Biblioteca</title>
        <link rel="stylesheet" href="Estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis|Ubuntu" rel="stylesheet"> 
    </head>
  
     <%String usuario=(String)session.getAttribute("usuario");
       %>
       <div id="banner">&emsp;¡Hola, <%=usuario%>!<a id="logout" href="logout.jsp">Cerrar Sesión</a></div>
       <div id="entrada">
       <div class="row">
       <form action="BibliotecaControlador" method="POST">
    <body background="background.jpg">
       <%String articulo=(String)request.getParameter("articulo");
       session.setAttribute("articulo",articulo);%>
        <%if(session!=null){
           List articulos=(List)session.getAttribute("busqueda");
           if(articulos!=null){
       %>
        <% for(Object o:articulos){
          ArticulosPOJO a=(ArticulosPOJO) o;
          String titulo=a.getTitulo();
          String id=a.getId_Articulo();
          String descripcion=a.getDescripcion();
          String url=a.getUrl_Imagen();
          int categoriaArt=a.getId_Categoria();
          if(id.equals(articulo)){
      %>
      <a href="consulta.jsp" title="Regresar"><img src="goback.png"></a>
          <h2><%=titulo%></h2>
          <div class="column">
                    <img src=<%=url%> width="200px">
          </div>
                    
          <div class="column">
                    Clave: <b><%=id%></b><br><br>
                    Título: <input name="titulo" type="text" value="<%=titulo%>">
                    <br><br>
                    Descripción:<br><br> <textarea name="descripcion" rows="3" cols="54"><%=descripcion%></textarea>
                    <br><br>
                    Categoría: <select id="categorias" name="categorias">
                        <%CategoriasDAO c=new CategoriasDAODBImpl();
                        List categorias=c.mostrarTodas();
                        for(Object obj:categorias){
                            CategoriasPOJO categoria=(CategoriasPOJO)obj;
                          if(categoria.getIdCategoria()==categoriaArt){%>
                            <option selected="selected" value=<%=categoria.getIdCategoria()%>><%=categoria.getCategoria()%></option>
                         <% }
                          else{%>
                            <option value=<%=categoria.getIdCategoria()%>><%=categoria.getCategoria()%></option>
                          <%}%>  
                    <%}%>
                    </select> 
                    <br><br>
                        <input id="boton" type="submit" value="Eliminar" name="accion">
                        &emsp;<input id="boton" type="submit" value="Guardar" name="accion">
          </div>
                    </div>
       </div>  
                   
             
               
                   
                
    <%  }
      }
           }
        }      
    %>                            
    </form>
    </body>
</html>
