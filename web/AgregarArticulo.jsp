<%-- 
    Document   : AgregarArticulo
    Created on : 21/04/2016, 07:14:44 PM
    Author     : Karina
--%>

<%@page import="biblioteca.CategoriasPOJO"%>
<%@page import="java.util.List"%>
<%@page import="biblioteca.CategoriasDAODBImpl"%>
<%@page import="biblioteca.CategoriasDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Algoteca</title>
        <link rel="stylesheet" href="Estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis|Ubuntu" rel="stylesheet"> 
    </head>
    
    <body background="background.jpg">
       <%String usuario=(String)session.getAttribute("usuario");
       %>
       <div id="banner">&emsp;¡Hola, <%=usuario%>!<a id="logout" href="logout.jsp">Cerrar Sesión</a></div>
       <form id="entrada" action="BibliotecaControlador" method="POST">
        <a href="consulta.jsp" title="Regresar"><img src="goback.png"></a>
        <h2>Registro de artículo</h2>
        <br>
        <div id="registroArt">
        &emsp;&emsp;Clave: <input type="text" name="clave" required>
        <br><br>
        &emsp;&emsp;Título: <input type="text" name="titulo" required>
        <br><br>
        &emsp;&emsp;Descripción: <textarea name="descripcion" rows="4" cols="60" required></textarea>
        <br><br>
        &emsp;&emsp;Categoría: 
         <select id="categorias" name="categoria" required>
         <option value="">Seleccionar categoría</option>
         <%CategoriasDAO c=new CategoriasDAODBImpl();
           List categorias=c.mostrarTodas();
           for(Object o:categorias){
               CategoriasPOJO categoria=(CategoriasPOJO)o;
         %>
         <option value="<%=categoria.getIdCategoria()%>"><%=categoria.getCategoria()%></option>
        <%}%>
        </select>
        <br><br>
        &emsp;&emsp;URL: <input type="text" name="url" required>
        &emsp;&emsp;&emsp;
        <input type="hidden" name="accion" value="guardarArticulo">
        <input id="boton" type="submit" value="Guardar">
        <br><br>
        </div>
    </form>
    </body>
</html>
