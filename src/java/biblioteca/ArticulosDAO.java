/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Karina
 */
public interface ArticulosDAO {
    public void insertar(ArticulosPOJO a) throws SQLException;
    public List buscar(ArticulosPOJO a)throws SQLException;
    public boolean existe(ArticulosPOJO a)throws SQLException;
    public void eliminar(ArticulosPOJO a)throws SQLException;
    public void modificar(ArticulosPOJO a)throws SQLException;
    
}
