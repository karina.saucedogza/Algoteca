/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karina
 */
public class ArticulosDAODBImpl implements ArticulosDAO {
    private Connection conexion;
   
    private void abrirConexion() throws SQLException, IOException{
        Properties prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();    
        InputStream stream = loader.getResourceAsStream("/datos.properties");
        prop.load(stream);
        String uri=prop.getProperty("dbURL");
        String usuario=prop.getProperty("dbUser");
        String contraseña=prop.getProperty("dbPassword");
        conexion=DriverManager.getConnection(uri,usuario,contraseña);
    }
    private void cerrarConexion() throws SQLException{
        conexion.close();
    }
    public void insertar(ArticulosPOJO articulo) throws SQLException{
        String id_Articulo=articulo.getId_Articulo();
        String titulo=articulo.getTitulo();
        String descripcion=articulo.getDescripcion();
        String url_Imagen=articulo.getUrl_Imagen();
        String usuario=articulo.getUsuario();
        int id_Categoria=articulo.getId_Categoria();
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(ArticulosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String insertar="INSERT into ARTICULOS values(?,?,?,?,?,?)";
        PreparedStatement stmt=conexion.prepareStatement(insertar);
        stmt.setString(1,id_Articulo);
        stmt.setString(2,titulo);
        stmt.setString(3,descripcion);
        stmt.setString(4,url_Imagen);
        stmt.setInt(5,id_Categoria);
        stmt.setString(6,usuario);
        stmt.executeUpdate();
        cerrarConexion();        
    }
    public List buscar(ArticulosPOJO articulo) throws SQLException{
        String busqueda="%"+articulo.getTitulo()+"%";
        String usuario=articulo.getUsuario();
        List lista=new ArrayList();
        ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(ArticulosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String buscar="SELECT * from ARTICULOS where upper(TITULO) LIKE upper(?) and upper(USUARIO)=upper(?)";
        PreparedStatement stmt=conexion.prepareStatement(buscar);
        stmt.setString(1,busqueda);
        stmt.setString(2,usuario);
        rs=stmt.executeQuery();
        while(rs.next()){
            String id_Articulo=rs.getString("ID_ARTICULO");
            String titulo=rs.getString("TITULO");
            String descripcion=rs.getString("DESCRIPCION");
            String url=rs.getString("URL_IMAGEN");
            int categoria=rs.getInt("ID_CATEGORIA");
            ArticulosPOJO articulos=new ArticulosPOJO();
            articulos.setId_Articulo(id_Articulo);
            articulos.setTitulo(titulo);
            articulos.setDescripcion(descripcion);
            articulos.setUrl_Imagen(url);
            articulos.setId_Categoria(categoria);
            lista.add(articulos);
        }
        cerrarConexion();
        return lista;     
    }
    public void eliminar(ArticulosPOJO articulo) throws SQLException{
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(ArticulosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String eliminar="DELETE from ARTICULOS where ID_ARTICULO=? and USUARIO=?";
        PreparedStatement stmt=conexion.prepareStatement(eliminar);
        stmt.setString(1,articulo.getId_Articulo());
        stmt.setString(2, articulo.getUsuario());
        stmt.executeUpdate();
        cerrarConexion();
    }
    public void modificar(ArticulosPOJO articulo) throws SQLException{
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(ArticulosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String modificar="UPDATE ARTICULOS SET TITULO=?, DESCRIPCION=?, ID_CATEGORIA=? where ID_ARTICULO=? and USUARIO=?";
        PreparedStatement stmt=conexion.prepareStatement(modificar);
        stmt.setString(1,articulo.getTitulo());
        stmt.setString(2,articulo.getDescripcion());
        stmt.setInt(3,articulo.getId_Categoria());
        stmt.setString(4,articulo.getId_Articulo());
        stmt.setString(5,articulo.getUsuario());
        stmt.executeUpdate();
        cerrarConexion();
        
    }
    
    public boolean existe(ArticulosPOJO articulo) throws SQLException{
       boolean existe=false;
        int idArt=Integer.parseInt(articulo.getId_Articulo());
        String user=articulo.getUsuario();
        ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(CategoriasDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String buscar="SELECT * from ARTICULOS where ID_ARTICULO=? and USUARIO=?";
        PreparedStatement stmt=conexion.prepareStatement(buscar);
        stmt.setInt(1,idArt);
        stmt.setString(2,user);
        rs=stmt.executeQuery();
        if(rs.next()){
            existe=true;
        }
        cerrarConexion();
        return existe;
    }

   
   
    
}
