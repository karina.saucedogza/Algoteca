/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Principal
 */
public interface CategoriasDAO {
    public void insertarCategoria(CategoriasPOJO categoria) throws SQLException; 
    public List mostrarTodas()throws SQLException;
    public boolean existe(CategoriasPOJO c)throws SQLException;
}
