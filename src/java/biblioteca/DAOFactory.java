/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

/**
 *
 * @author Karina Saucedo
 */
public class DAOFactory {
    public ArticulosDAO crearArticuloDAO(){
        return new ArticulosDAODBImpl();
    }
    public CategoriasDAO crearCategoriaDAO(){
        return new CategoriasDAODBImpl();
    }
    public UsuariosDAO crearUsuarioDAO(){
        return new UsuariosDAODBImpl();
    }
}
