/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

/**
 *
 * @author Karina
 */
public class ArticulosPOJO {
    private String id_Articulo;
    private String titulo;
    private String descripcion;
    private String url_Imagen;
    private int id_Categoria;
    private String usuario;

    /**
     * @return the id_Articulo
     */
    public String getId_Articulo() {
        return id_Articulo;
    }

    /**
     * @param id_Articulo the id_Articulo to set
     */
    public void setId_Articulo(String id_Articulo) {
        this.id_Articulo = id_Articulo;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the url_Imagen
     */
    public String getUrl_Imagen() {
        return url_Imagen;
    }

    /**
     * @param url_Imagen the url_Imagen to set
     */
    public void setUrl_Imagen(String url_Imagen) {
        this.url_Imagen = url_Imagen;
    }

    /**
     * @return the id_Categoria
     */
    public int getId_Categoria() {
        return id_Categoria;
    }

    /**
     * @param id_Categoria the id_Categoria to set
     */
    public void setId_Categoria(int id_Categoria) {
        this.id_Categoria = id_Categoria;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
}
