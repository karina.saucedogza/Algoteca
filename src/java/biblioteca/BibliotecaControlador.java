/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Karina
 */
public class BibliotecaControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String accion=request.getParameter("accion");
        if(accion.equals("login")){
            String user=request.getParameter("usuario");
            String pass=request.getParameter("pass");
            DAOFactory factory=new DAOFactory();
            UsuariosDAO dao=factory.crearUsuarioDAO();
            UsuariosPOJO u=new UsuariosPOJO();
            u.setUsuario(user);
            u.setPassword(pass);
            boolean login=dao.buscar(u);
            if(login==true){
                HttpSession sesion=request.getSession();
                sesion.setAttribute("usuario",user);
                response.sendRedirect("consulta.jsp");
            }
            else{
                response.sendRedirect("index.html");
            }
        }
        if(accion.equals("registro")){
            String user=request.getParameter("usuario");
            String pass=request.getParameter("pass");
            String name=request.getParameter("nombre");
            DAOFactory factory=new DAOFactory();
            UsuariosDAO dao=factory.crearUsuarioDAO();
            UsuariosPOJO u=new UsuariosPOJO();
            u.setNombre(name);
            u.setUsuario(user);
            u.setPassword(pass);
            boolean existe=dao.existe(u);
            if(existe==true){
                response.sendRedirect("error.jsp");
            }
            else{
                dao.insertar(u);
                response.sendRedirect("index.html");
            }
        }
        if(accion.equals("guardarArticulo")){
            String id_Articulo=request.getParameter("clave");
            String titulo=request.getParameter("titulo");
            String descripcion=request.getParameter("descripcion");
            String url_Imagen=request.getParameter("url");
            int id_Categoria=Integer.parseInt(request.getParameter("categoria"));
            HttpSession sesion=request.getSession();
            String usuario=(String)sesion.getAttribute("usuario");
            DAOFactory factory=new DAOFactory();
            ArticulosDAO dao=factory.crearArticuloDAO();
            ArticulosPOJO a=new ArticulosPOJO();
            a.setId_Articulo(id_Articulo);
            a.setTitulo(titulo);
            a.setDescripcion(descripcion);
            a.setUrl_Imagen(url_Imagen);
            a.setId_Categoria(id_Categoria);
            a.setUsuario(usuario);
            boolean existe=dao.existe(a);
            if(existe){
                response.sendRedirect("error.jsp");
            }
            else{
                dao.insertar(a);
                response.sendRedirect("consulta.jsp");
            }
        }
        if(accion.equals("busqueda")){
            String titulo=request.getParameter("busqueda");
            HttpSession sesion=request.getSession();
            String usuario=(String)sesion.getAttribute("usuario");
            DAOFactory factory=new DAOFactory();
            ArticulosDAO dao=factory.crearArticuloDAO();
            ArticulosPOJO a=new ArticulosPOJO();
            a.setTitulo(titulo);
            a.setUsuario(usuario);
            List resultados=dao.buscar(a);
            sesion.setAttribute("busqueda", resultados);
            response.sendRedirect("consulta.jsp");
        }
        if(accion.equals("registroCategoria")){
            int id=Integer.parseInt(request.getParameter("clave"));
            String nombre=request.getParameter("nombre");
            DAOFactory factory=new DAOFactory();
            CategoriasDAO dao=factory.crearCategoriaDAO();
            CategoriasPOJO c=new CategoriasPOJO();
            c.setIdCategoria(id);
            c.setCategoria(nombre);
            boolean existe=dao.existe(c);
            if(existe==true){
                response.sendRedirect("error.jsp");
            }
            else{
                dao.insertarCategoria(c);
                response.sendRedirect("consulta.jsp");
            }
        }
        if(accion.equals("Eliminar")){
            HttpSession sesion=request.getSession();
            String usuario=(String)sesion.getAttribute("usuario");
            String articulo=(String)sesion.getAttribute("articulo");
            DAOFactory factory=new DAOFactory();
            ArticulosDAO dao=factory.crearArticuloDAO();
            ArticulosPOJO a=new ArticulosPOJO();
            a.setUsuario(usuario);
            a.setId_Articulo(articulo);
            dao.eliminar(a);
            sesion.removeAttribute("busqueda");
            response.sendRedirect("mensaje.jsp");           
        }
        if(accion.equals("Guardar")){
            HttpSession sesion=request.getSession();
            String usuario=(String)sesion.getAttribute("usuario");
            String articulo=(String)sesion.getAttribute("articulo");
            String titulo=request.getParameter("titulo");
            String descripcion=request.getParameter("descripcion");
            int id_Categoria=Integer.parseInt(request.getParameter("categorias"));
            DAOFactory factory=new DAOFactory();
            ArticulosDAO dao=factory.crearArticuloDAO();
            ArticulosPOJO a=new ArticulosPOJO();
            a.setUsuario(usuario);
            a.setId_Articulo(articulo);
            a.setTitulo(titulo);
            a.setDescripcion(descripcion);
            a.setId_Categoria(id_Categoria);
            dao.modificar(a);
            sesion.removeAttribute("busqueda");
            response.sendRedirect("consulta.jsp");
        }
         
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BibliotecaControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BibliotecaControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
public String limpiar(String campo) throws UnsupportedEncodingException{
        String cadena;
        cadena=URLEncoder.encode(campo,"UTF-8");
        return cadena;
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(BibliotecaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(BibliotecaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
