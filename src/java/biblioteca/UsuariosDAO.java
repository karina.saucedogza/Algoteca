/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.sql.SQLException;

/**
 *
 * @author Principal
 */
public interface UsuariosDAO {
    public void insertar(UsuariosPOJO usuario) throws SQLException;
    public boolean buscar(UsuariosPOJO usuario) throws SQLException;
    public boolean existe(UsuariosPOJO usuario) throws SQLException;
    
}
