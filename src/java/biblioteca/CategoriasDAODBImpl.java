/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karina
 */
public class CategoriasDAODBImpl implements CategoriasDAO{
    private Connection conexion;
    private void abrirConexion() throws SQLException, IOException{
        Properties prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();    
        InputStream stream = loader.getResourceAsStream("/datos.properties");
        prop.load(stream);
        String uri=prop.getProperty("dbURL");
        String usuario=prop.getProperty("dbUser");
        String contraseña=prop.getProperty("dbPassword");
        conexion=DriverManager.getConnection(uri,usuario,contraseña);
    }
    private void cerrarConexion() throws SQLException{
        conexion.close();
    }
    public void insertarCategoria(CategoriasPOJO categorias)throws SQLException{
        int id=categorias.getIdCategoria();
        String categoria=categorias.getCategoria();
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(CategoriasDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String insertar="INSERT into CATEGORIAS values(?,?)";
        PreparedStatement stmt=conexion.prepareStatement(insertar);
        stmt.setInt(1,id);
        stmt.setString(2,categoria);
        stmt.executeUpdate();
        cerrarConexion();  
    }
   public List mostrarTodas()throws SQLException{
       List categorias=new ArrayList();
       ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(CategoriasDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       String mostrar="SELECT * from CATEGORIAS";
       PreparedStatement stmt=conexion.prepareStatement(mostrar);
       rs=stmt.executeQuery();
       while(rs.next()){
           int id=rs.getInt("ID_CATEGORIA");
           String nombre=rs.getString("CATEGORIA");
           CategoriasPOJO c=new CategoriasPOJO();
           c.setIdCategoria(id);
           c.setCategoria(nombre);
           categorias.add(c);
       }
       return categorias;
   }
   public boolean existe(CategoriasPOJO categorias)throws SQLException{
       boolean existe=false;
        int categoria=categorias.getIdCategoria();
        String nombre=categorias.getCategoria();
        ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(CategoriasDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String buscar="SELECT * from CATEGORIAS where ID_CATEGORIA=? or CATEGORIA=?";
        PreparedStatement stmt=conexion.prepareStatement(buscar);
        stmt.setInt(1,categoria);
        stmt.setString(2,nombre);
        rs=stmt.executeQuery();
        if(rs.next()){
            existe=true;
        }
        cerrarConexion();
        return existe;
   }
   
}
