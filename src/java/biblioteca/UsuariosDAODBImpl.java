/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package biblioteca;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Karina
 */
public class UsuariosDAODBImpl implements UsuariosDAO{
    private Connection conexion;
   
    private void abrirConexion() throws SQLException, IOException{
        Properties prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();    
        InputStream stream = loader.getResourceAsStream("/datos.properties");
        prop.load(stream);
        String uri=prop.getProperty("dbURL");
        String usuario=prop.getProperty("dbUser");
        String contraseña=prop.getProperty("dbPassword");
        conexion=DriverManager.getConnection(uri,usuario,contraseña);
    }
    private void cerrarConexion() throws SQLException{
        conexion.close();
    }
    public void insertar(UsuariosPOJO usuario) throws SQLException{
        String nombre=usuario.getNombre();
        String username=usuario.getUsuario();
        int password=usuario.getPassword().hashCode();
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(UsuariosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String insertar="INSERT into USUARIOS values(?,?,?)";
        PreparedStatement stmt=conexion.prepareStatement(insertar);
        stmt.setString(1,username);
        stmt.setInt(2,password);
        stmt.setString(3,nombre);
        stmt.executeUpdate();
        cerrarConexion();        
    }
    public boolean buscar(UsuariosPOJO usuario) throws SQLException{
        boolean login=false;
        ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(UsuariosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String buscar="SELECT * from USUARIOS where USUARIO=? and PASSWORD=?";
        PreparedStatement stmt=conexion.prepareStatement(buscar);
        stmt.setString(1,usuario.getUsuario());
        stmt.setInt(2,usuario.getPassword().hashCode());
        rs=stmt.executeQuery();
        while(rs.next()){
            String name=rs.getString("NOMBRE");
            String user=rs.getString("USUARIO");
            String pass=rs.getString("PASSWORD");
            UsuariosPOJO usuarios=new UsuariosPOJO();
            usuarios.setNombre(name);
            usuarios.setUsuario(user);
            usuarios.setPassword(pass);
            if(usuarios.getUsuario()!=null && usuarios.getPassword()!=null){
                login=true;
            }
        }
        cerrarConexion();
        return login;
    }
    public boolean existe(UsuariosPOJO usuarios)throws SQLException{
        boolean existe=false;
        ResultSet rs;
        try {
            abrirConexion();
        } catch (IOException ex) {
            Logger.getLogger(UsuariosDAODBImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        String buscar="SELECT * from USUARIOS where USUARIO=?";
        PreparedStatement stmt=conexion.prepareStatement(buscar);
        stmt.setString(1, usuarios.getUsuario());
        rs=stmt.executeQuery();
        while(rs.next()){
            String nombre=rs.getString("NOMBRE");
            String user=rs.getString("USUARIO");
            String pass=rs.getString("PASSWORD");
            UsuariosPOJO u=new UsuariosPOJO();
            u.setNombre(nombre);
            u.setUsuario(user);
            u.setPassword(pass);
            if(usuarios.getUsuario()!=null && usuarios.getPassword()!=null && usuarios.getNombre()!=null){
                existe=true;
            }
        }
        cerrarConexion();
        return existe;
    }
}
